package demo.webApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Точка входа в приложение.
 */
@SpringBootApplication
public class WebApiApplication {

	/**
	 * Точка входа в приложение.
	 * @param args дополнительные аргументы.
	 */
	public static void main(String[] args) {
		SpringApplication.run(WebApiApplication.class, args);
	}

}
