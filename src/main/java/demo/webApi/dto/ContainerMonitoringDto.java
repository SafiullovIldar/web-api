package demo.webApi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Дто с информацией о контейнере и датчиках установленных на нём.
 */
@Getter
@RequiredArgsConstructor
public class ContainerMonitoringDto {

    /**
     * Идентификатор контейнера. */
    @JsonProperty("container_id")
    private final Integer containerId;

    /**
     * Идентификатор датчика. */
    @JsonProperty("sensor_id")
    private final Integer sensorId;

    /**
     * Показание датчика. */
    @JsonProperty("indication")
    private final String indication;

    /**
     * Тип датчика. */
    @JsonProperty("sensor_type")
    private final String sensorType;
}
