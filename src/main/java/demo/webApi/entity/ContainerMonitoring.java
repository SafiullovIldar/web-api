package demo.webApi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Сущность мониторинга за состоянием контейнеров.
 */
@Setter
@Getter
@Entity
@Table(name = "container_monitoring")
public class ContainerMonitoring {

    /**
     * Идентификатор сущности. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Идентификатор контейнера. */
    @Column(name = "container_id")
    private Integer containerId;

    /**
     * Идентификатор датчика. */
    @Column(name = "sensor_id")
    private Integer sensorId;

    /**
     * Показание датчика. */
    @Column(name = "indication")
    private String indication;

    /**
     * Тип датчика. */
    @Column(name = "sensor_type")
    private String sensorType;
}
