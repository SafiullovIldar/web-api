package demo.webApi.entity.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import demo.webApi.services.exceptions.UnsupportedSensorTypeException;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public enum SensorType {

    TEMPERATURE("TEMPERATURE"),
    SHOCK("SHOCK"),
    HUMIDITY("HUMIDITY");

    /**
     * Строковое представление текущей enum-константы (тип датчика),
     */
    @NonNull
    private String value;

    /**
     * @return строковое представление текущей enum-константы.
     */
    @JsonValue
    public String getValue() {
        return value;
    }

    /**
     * @param type тип датчика.
     * @return строкове представление enum константы.
     */
    public static String getStringEnumValue(@NonNull final String type) {
        if (type.equalsIgnoreCase(TEMPERATURE.value)) {
            return SensorType.TEMPERATURE.value;
        }
        else if (type.equalsIgnoreCase(SHOCK.value)) {
            return SensorType.SHOCK.value;
        }
        else if (type.equalsIgnoreCase(HUMIDITY.value)) {
            return SensorType.HUMIDITY.value;
        } else {
            throw new UnsupportedSensorTypeException("Не поддерживаемый тип датчика.");
        }
    }
}

