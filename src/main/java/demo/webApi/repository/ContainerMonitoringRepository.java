package demo.webApi.repository;

import demo.webApi.entity.ContainerMonitoring;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Предоставляет интерфейс взаимодействия с таблицей container_monitoring.
 */
@Repository
@Transactional(rollbackOn = Throwable.class)
public interface ContainerMonitoringRepository extends PagingAndSortingRepository<ContainerMonitoring, Integer> {

    /**
     * @param containerId идентификатор контейнера.
     * @param pageable параметры пагинации.
     * @return страница содержащая информацию о контейнере.
     */
    Page<ContainerMonitoring> findByContainerId(Integer containerId, Pageable pageable);

    /**
     * @param sensorType тип датчика.
     * @param pageable параметры пагинации.
     * @return страница содержащая информацию о контейнере.
     */
    Page<ContainerMonitoring> findBySensorType(String sensorType, Pageable pageable);
}
