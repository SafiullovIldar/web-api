package demo.webApi.resources;

import demo.webApi.dto.ContainerMonitoringDto;
import demo.webApi.services.ContainerMonitoringService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

/**
 * Rest-контроллер для основного функционала работы с информацией о контейнерах.
 */
@RestController
@RequiredArgsConstructor
public class ContainerMonitoringController {

    /**
     * Сервис для взаимодествия с сущностью {@link demo.webApi.entity.ContainerMonitoring}. */
    @NonNull private final ContainerMonitoringService containerMonitoringService;

    /**
     * @param pageable параметры пагинации.
     * @param id идентификатор контейнера.
     * @return информации о контейнере по заданному идентификатору.
     */
    @GetMapping(value = "/container/{id}")
    public Collection<ContainerMonitoringDto> getInformationAboutContainerById(
        @PageableDefault @NonNull final Pageable pageable,
        @PathVariable("id") @NonNull final Integer id)
    {
        final List<ContainerMonitoringDto> dto = containerMonitoringService.findByContainerId(id, pageable);

        return dto;
    }

    /**
     * @param pageable параметры пагинации.
     * @param type тип датчика.
     * @return информации о датчиках по заданному типу.
     */
    @GetMapping(value = "/sensor_type/{type}")
    public Collection<ContainerMonitoringDto> getInformationAboutSensorsByType(
        @PageableDefault @NonNull final Pageable pageable,
        @PathVariable("type") @NonNull final String type)
    {
        final List<ContainerMonitoringDto> dto = containerMonitoringService.findBySensorType(type, pageable);

        return dto;
    }
}
