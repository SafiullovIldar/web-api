package demo.webApi.services;

import demo.webApi.dto.ContainerMonitoringDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Сервис для взаимодествия с сущностью {@link demo.webApi.entity.ContainerMonitoring}.
 */
public interface ContainerMonitoringService {

    /**
     * @param containerId идетификатор контейнера.
     * @param pageable параметры пагинации.
     * @return дто с информацией о контейнере и датчиках установленных на нём.
     */
    List<ContainerMonitoringDto> findByContainerId(Integer containerId, Pageable pageable);

    /**
     * @param sensorType тип датчика.
     * @param pageable параметры пагинации.
     * @return дто с информацией о контейнере и датчиках установленных на нём.
     */
    List<ContainerMonitoringDto> findBySensorType(String sensorType, Pageable pageable);
}
