package demo.webApi.services.impl;

import demo.webApi.dto.ContainerMonitoringDto;
import demo.webApi.entity.ContainerMonitoring;
import demo.webApi.entity.enums.SensorType;
import demo.webApi.repository.ContainerMonitoringRepository;
import demo.webApi.services.ContainerMonitoringService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис для взаимодествия с сущностью {@link demo.webApi.entity.ContainerMonitoring}.
 */
@Service
@RequiredArgsConstructor
public class ContainerMonitoringServiceImpl implements ContainerMonitoringService {

    /**
     * Предоставляет интерфейс взаимодействия с таблицей container_monitoring. */
    @NonNull private final ContainerMonitoringRepository containerMonitoringRepository;

    @Override
    public List<ContainerMonitoringDto> findByContainerId(
        @NonNull final Integer containerId,
        @NonNull final Pageable pageable)
    {
        final Page<ContainerMonitoring> paging = containerMonitoringRepository.findByContainerId(containerId, pageable);
        return checkContentAndReturnIfPresent(paging);
    }

    @Override
    public List<ContainerMonitoringDto> findBySensorType(@NonNull final String sensorType,
                                                         @NonNull final Pageable pageable)
    {
        final String type = SensorType.getStringEnumValue(sensorType);
        final Page<ContainerMonitoring> paging = containerMonitoringRepository.findBySensorType(type, pageable);

        return checkContentAndReturnIfPresent(paging);
    }

    /**
     * @param paging параметры пагинации.
     * @return коллекцию дто с информацией о контейнере и датчиках установленных на нём, если по заданным параметрам
     * информации не нашлось выбрасывает исключение EntityNotFoundException.
     */
    private List<ContainerMonitoringDto> checkContentAndReturnIfPresent(
        @NonNull final Page<ContainerMonitoring> paging)
    {
        if (paging.hasContent()) {
            final List<ContainerMonitoringDto> monitoringDto = convertEntityToDto(paging);
            return monitoringDto;
        } else {
            final String exceptionMessage = "Неверные входные параметры.";
            throw new EntityNotFoundException(exceptionMessage);
        }
    }

    private List<ContainerMonitoringDto> convertEntityToDto(@NonNull final Page<ContainerMonitoring> paging) {
        return paging.get()
            .map(containerMonitoring -> {
                final Integer id = containerMonitoring.getContainerId();
                final Integer sensorId = containerMonitoring.getSensorId();
                final String indication = containerMonitoring.getIndication();
                final String sensorType = containerMonitoring.getSensorType();
                ContainerMonitoringDto dto = new ContainerMonitoringDto(id, sensorId, indication, sensorType);
                return dto;
            }).collect(Collectors.toList());
    }
}
